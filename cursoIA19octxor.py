import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers.core import Dense
entradas=np.array([[0,0],[0,1],[1,0],[1,1]],"float32")

salidas=np.array([[0],[1],[1],[0]],"float32")

model=Sequential()
model.add(Dense(16,input_dim=2,activation='relu'))
model.add(Dense(1,activation='sigmoid'))

model.compile(loss="mean_squared_error", optimizer='adam',metrics=['binary_accuracy'])
historial=model.fit(entradas,salidas,epochs=1500)

plt.plot(historial.history["loss"])
plt.title('Proceso de Perdidas')
plt.xlabel('Epocas')
plt.ylabel('Perdidas')
plt.show()
print ("Predicción::::::")
print(model.predict([[1,0]]).round())